# Development

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). You can find the most recent version of its guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

After cloning repository you need to get all needed packages:

```sh
npm install
```

In the project directory, you can run:

```sh
npm run start
```

It runs the app in the development mode.
Open [http://localhost:8000](http://localhost:8000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Flow

[Flow](https://flow.org/) is a static type checker for JavaScript. Maybe you will find this [flow-node-boilerplate](https://github.com/asci/flow-node-boilerplate) interesting.

Validate types in your code:

```sh
npm run flow
```

It checks entire project expect ignored folders/files. Take a look at the `.flowconfig` file to see what is ignored.

To verify current flow coverage percent run:

```sh
npm run flow:coverage
```

It prints coverage table into your console. More use friendly version does output into `flow-coverage` folder.

Covering takes into account only `src` folder. More settings regarding the flow coverage you can find in `.flowcoverage` file.

Building flow:

```sh
npm run flow:build
```

It actually doesn't build anything. It just removes type defintions from source files and output files into `src-clean` folder.

Flow doesn't know dependencies types out of the box. We need to install them separately:

```sh
npm run flow:deps
```

It saves libdefs into `flow-typed` folder. Not having them will probably cause type checking fail.

## ESLint

[ESLint](https://eslint.org/) is pluggable linting utility for JavaScript. Its configuration and rules are located in `.eslintrc` file.

To run ESLint use the following command:

```sh
npm run lint
```

It will perform eslint checks on all files inside `src` and `test` folder.

Some of the warnings are potentially auto fixable with:

```sh
npm run lint:fix
```

## Testing

```sh
npm run test
```

Launches the test runner in the interactive watch mode.

```sh
npm run build
```

Builds the app for production to the `build` folder.

# App

We are using [Apache Cordova](https://cordova.apache.org/) to build mobile app.

## Cordova

After cloning repository you will be probably getting a "No platforms added to this project" error. Running next command will fill out the platforms and plugins directories with source files needed for running and building cordova:

```sh
cordova prepare
```

To test this app on your android device you need to run:

```sh
cordova run android
```

It finds your android device, installs and opens the app on it. Don't forget to enable USB debugging on your android phone.
