# Cordova Hooks

Cordova Hooks represent special scripts which could be added to customize cordova commands. We are using three hooks which are executed automatically before running cordova command:

1. Building react app.
2. Moving files to www folder.
3. Addding cordova script tag to index.html file.

Explanation: Cordova serves files located in www folder which is empty by default. These hooks take care to prepare react build, move it to www folder and equip it with necessary cordova script.
