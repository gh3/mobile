#!/usr/bin/env node

var fs = require('fs')

var indexFilePath = 'www/index.html'
var cordovaFileName = 'cordova.js'
var cordovaScriptTag = '<script type="text/javascript" src="' + cordovaFileName + '"></script>'

var indexHtml = fs.readFileSync(indexFilePath, 'utf-8')
if (indexHtml.indexOf(cordovaFileName) > -1) {
  throw '\nThe ' + cordovaFileName + ' was found in ' + indexFilePath + ' file!\nMost likely this will cause an error in next step so the process is hard stopped.\nYou have to manually check what causes that ' + cordovaFileName + ' has appeared in ' + indexFilePath + ' file.'
}

var firstScriptLocation = indexHtml.indexOf('<script')
if (firstScriptLocation === -1) {
  throw '\nThere is not any script tag in ' + indexFilePath + ' file.\nMost likely this is an error and also current hook is lost where to append ' + cordovaFileName + ' script tag.\nYou have to manually check what has happened with ' + indexFilePath + ' file.'
}

var newIndexHtml = indexHtml.slice(0, firstScriptLocation) + cordovaScriptTag + indexHtml.slice(firstScriptLocation)
fs.writeFileSync(indexFilePath, newIndexHtml, 'utf-8')

console.log('\nSuccessfully added ' + cordovaFileName + ' script to ' + indexFilePath + ' file.\n')
