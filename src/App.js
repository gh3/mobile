import React, { Component } from 'react'
import MinioUploader from './components/MinioUploader'

type AppProps = {}

type AppState = {}

class App extends Component<AppProps, AppState> {
  render() {
    return (
      <div className="App">
        <span>Hello World!</span>
        <MinioUploader />
      </div>
    )
  }
}

export default App
