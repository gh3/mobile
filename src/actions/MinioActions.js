import { Gateway } from '../Constants'

export const minioPresignedUrl = async (bucket: string, fileName: string): Promise<string> => {
  const response = await fetch(`${Gateway.URL}/minio/presigned-url/${bucket}/${fileName}/`)
  const json = await response.json()
  return json.url ? json.url : ''
}

export const uploadFileToMinio = (uploadUrl: string, file: File): void => {
  fetch(uploadUrl, {
    method: 'PUT',
    body: file
  })
}
