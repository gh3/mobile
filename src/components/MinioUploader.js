import React, { Component } from 'react'
import { minioPresignedUrl, uploadFileToMinio } from '../actions/MinioActions'

type MinioUploaderProps = {}

type MinioUploaderState = {}

class MinioUploader extends Component<MinioUploaderProps, MinioUploaderState> {
  handleFileInputChange(event: SyntheticEvent<HTMLInputElement>) {
    const { files } = event.currentTarget
    for (let i = 0; i < files.length; i++) {
      this.handleFile(files[i])
    }
  }

  async handleFile(file: File) {
    const bucket = 'uploads'
    const uploadUrl = await minioPresignedUrl(bucket, file.name)
    uploadFileToMinio(uploadUrl, file)
  }

  render() {
    return (
      <div>
        <input type="file" multiple={ true } onChange={ event => this.handleFileInputChange(event) }/>
      </div>
    )
  }
}

export default MinioUploader
