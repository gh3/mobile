import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'

const startApp: Function = () => {
  const rootElement: ?HTMLElement = document.getElementById('root')
  if (rootElement) {
    ReactDOM.render(<App />, rootElement)
  } else {
    window.console.error('Root element is missing!')
  }
}

if (!window.cordova) {
  startApp()
} else {
  document.addEventListener('deviceready', startApp, false)
}
